var userStr = sessionStorage.getItem("user");
var user;
if (userStr) {
    user = JSON.parse(userStr);
} else {
    window.location.href = "/login.html";
}