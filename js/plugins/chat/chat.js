/*发送消息*/
function send(headUrl, str, time) {
    var html = "<div class='send'><div class=\"time\">" + time + "</div><div class='msg'><img src=" + headUrl + " />" +
        "<p><i class='msg_input'></i>" + str + "</p></div></div>";
    upView(html);
}

/*接受消息*/
function show(headUrl, str, time) {
    var html = "<div class='show'><div class=\"time\">" + time + "</div><div class='msg'><img src=" + headUrl + " />" +
        "<p><i class='msg_input'></i>" + str + "</p></div></div>";
    upView(html);
}

/*更新视图*/
function upView(html) {
    $('.message').append(html);
    $('body').animate({scrollTop: $('.message').outerHeight() - window.innerHeight}, 200)
}

function sj() {
    return parseInt(Math.random() * 10)
}

//格式化时间
function CurentTime(date) {
    var now = new Date();
    var weekday = ["周日", "周一", "周二", "周三", "周四", "周五", "周六"];
    var month = date.getMonth();
    var day = date.getDate();
    var hh = date.getHours();

    var mm = date.getMinutes();
    //两个时间相差的毫秒数.
    var cut = now.getTime() - date.getTime();

    if (cut <= 60 * 1000 * 24) {
        if (0 <= hh && hh <= 6) {
            return "凌晨" + hh + "时" + mm + "分";
        }
        if (6 < hh && hh < 12) {
            return "早上" + hh + "时" + mm + "分";
        }
        if (12 <= hh && hh < 18) {
            return "下午" + hh + "时" + mm + "分";
        }
        if (18 <= hh && hh <= 24) {
            return "晚上" + hh + "时" + mm + "分";
        }
    } else if (60 * 1000 * 24 < cut <= 60 * 1000 * 24 * 2) {
        if (0 <= hh && hh <= 6) {
            return "昨天" + hh + "时" + mm + "分";
        }
        if (6 < hh && hh < 12) {
            return "昨天" + hh + "时" + mm + "分";
        }
        if (12 <= hh && hh < 18) {
            return "昨天" + hh + "时" + mm + "分";
        }
        if (18 < hh && hh < 24) {
            return "昨天" + hh + "时" + mm + "分";
        }
    } else if (60 * 1000 * 24 * 2 <= cut <= 60 * 1000 * 24 * 7) {
        return weekday[day] + " " + hh + "时" + mm + "分";

    } else if (cut > 60 * 1000 * 24 * 7) {
        return month + "月" + day + "日 " + hh + "时" + mm + "分"
    }
}

//格式化时间
function CurentTimeWithSeconds(date) {
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hh = date.getHours();
    var mm = date.getMinutes();
    var ss = date.getSeconds();
    var clock = year + "-";
    if (month < 10)
        clock += "0";
    clock += month + "-";
    if (day < 10)
        clock += "0";
    clock += day + " ";
    if (hh < 10)
        clock += "0";
    clock += hh + ":";
    if (mm < 10) clock += '0';
    clock += mm + ":";
    if (ss < 10) clock += "0";
    clock += ss;

    return clock;
}

//跳转到底部
function end() {
    var c = window.document.body.scrollHeight;
    window.scroll(0, c);
}

$(function () {
    $('.footer').on('keyup', 'input', function () {
        if ($(this).val().length > 0) {
            $(this).next().css('background', '#114F8E').prop('disabled', true);

        } else {
            $(this).next().css('background', '#ddd').prop('disabled', false);
        }
    })

    //当前用户,即为发送者
    var senderId = user.id;

    //接收者id
    var receiverId = getParams().receiverId;

    $.get(baseUrl + "/users/" + receiverId, function (data) {
        var userdata = data.data;
        $("#sendImg").attr("src", userdata.headImgUrl);
        $(".receiverName").html(userdata.nickName);
        $(".receiverData").click(function () {
            window.location.href = "/userProfiles.html?id=" + receiverId;
        });
    });

    //发送请求,通过发送者id和接受者id获取到两者之间的信息
    $.get(baseUrl + "/messages/" + senderId + "/" + receiverId, function (data) {
        $.ajax({
            url: baseUrl + "/messages/" + senderId + "/" + receiverId,
            method: "put",
            success: function (data) {
                console.log(data);
            }
        });
        //逆序排列.
        data.list.reverse();
        $.each(data.list, function (index, ele) {

            //通过结果中发送者的id判断哪个是当前用户,是当前用户,就显示在右边
            if (ele.author_id == senderId) {
                $(".message").append('<div class="show">\n' +
                    '      <div class="time">' + CurentTime(new Date(ele.create_time)) + '</div>\n' +
                    '      <div class="msg">\n' +
                    '        <img src="' + ele.au_headImgUrl + '" width="40px" alt="" />\n' +
                    '        <p><i class="msg_input"></i>' + ele.content + '</p>\n' +
                    '      </div>\n' +
                    '    </div>');
            } else {
                $(".message").append('<div class="send">\n' +
                    '      <div class="time">' + CurentTime(new Date(ele.create_time)) + '</div>\n' +
                    '      <div class="msg">\n' +
                    '        <img src="' + ele.au_headImgUrl + '" width="40px" alt="" />\n' +
                    '        <p><i class="msg_input"></i>' + ele.content + '</p>\n' +
                    '      </div>\n' +
                    '    </div>');
            }
        });
        end()
    });

    //给发送按钮绑定点击事件
    $("#sendBtn").click(function () {
        var message = $("[name='message']").val();
        if (message == "") {
            //如果没有信息,直接返回
            $(document).dialog({
                type: 'notice',
                content: '<span class="info-text">内容不能为空</span>',
                autoClose: 1000,
                position: 'bottom'
            });
            return;
        }
        // $.ajax({
        //     url :"/userChats",
        //     data:{"sender.id":senderId,"receiver.id":receiverId,"message":message},
        //     type:"POST",
        //     success:function (data) {
        //         if (data.success){
        //             location.reload();
        //         }
        //     }
        // });
        //发送完后清空信息框内容
        $("[name='message']").val("");
        $("[name='message']").next().css('background', '#ddd').prop('disabled', false);
    });
    //查看这个页面的时候设置这条信息状态为已读状态
    var newTime = CurentTimeWithSeconds(new Date());

    function getNewMessage() {
        $.get("/userChats/" + receiverId + "/" + senderId + "/" + newTime, function (data) {
            if (data.length > 0) {
                $.each(data, function (index, ele) {
                    send(ele.sender.headImgUrl, ele.message, CurentTime(new Date(ele.sendTime)));
                });
                end();
                //如果有新的消息,就刷新时间
                newTime = CurentTimeWithSeconds(new Date());
            }
        });
    }

    // window.setInterval(getNewMessage, 2000);
});
