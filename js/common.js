//获取url上的请求参数
function getParams() {
    //获取问号及问号后面的内容
    var url = window.location.search;
    var params = new Object();
    if (url.indexOf("?") != -1) {
        //截取问号后面的内容,再使用&分割多个属性
        var arr = url.substr(1).split("&");
        for (var i = 0; i < arr.length; i++) {
            //使用=分割为keyvalue
            var keyValue = arr[i].split("=");
            params[keyValue[0]] = keyValue[1];
        }
    }
    return params;
}
//设置数组字段转换
jQuery.ajaxSettings.traditional = true;
//设置全局跨域访问
$.ajaxSetup({
    crossDomain: true,
    xhrFields: {withCredentials: true}
});

var baseUrl = "http://localhost:8088/";

function getHref(item, value) {
    var url = $(item).data("url");
    $(item).attr("href", url + value)
}

//改变样式状态  点赞和收藏等显示效果
function changeClass(item,class1,class2) {
    if(item.hasClass(class1)){
        item.removeClass(class1);
        item.addClass(class2);
    }else{
        item.removeClass(class2);
        item.addClass(class1);
    }
}